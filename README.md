# Foodprint: decrease your food-related carbon footprint

![storyboard.png](https://bitbucket.org/repo/rxA5rp/images/1345475777-storyboard.png)

The aim of Foodprint is to raise awareness of the carbon footprint associated with groceries you buy every day and make suggestions on how to reduce your footprint. At the same time, the app helps users to discover new products and recipes. Based on purchase information from loyalty programs (Cumulus, SuperCard) or simply by scanning receipts, we establish a carbon footprint. The food print can then be compared to previous purchases or other people's footprint on social media. Moreover, the app makes suggestions on how to reduce the footprint through smarter purchases such as local or seasonal products.


## Input

To input data, the app user links the app with his loyalty program account (Cumulus, SuperCard, etc.) or scans purchase receipts.


## User interaction

The user can compare his carbon footprint over a period of time. Data is represented as easily readable and appealing charts. Moreover, by selecting a specific week, the users can see which products contribute strongly to the carbon consumption.

The user can share statistics on social networks and compete with other users to improve their food-related carbon footprint together.


## Product suggestions

The app makes product suggestions based on previous purchases. Product suggestions allow the user to discover new products and, at the same time, reduce his carbon footprint.

## Application screen mockup
![app-screens.png](https://bitbucket.org/repo/rxA5rp/images/1024033268-app-screens.png)

## Data

The app uses [Eaternity](http://www.eaternity.org/), a publicly available resource. 


## Team 7

  * David Graf
  * Fien Thoolen
  * Iselin Medhaug
  * Jochem Snuverink
  * Theres Scherrer
  * Thomas Thurnherr


## Installation

### Using `create-react-app` with React Router + Express.js

See : https://medium.com/@patriciolpezjuri/using-create-react-app-with-react-router-express-js-8fa658bf892d

### Development

Clone this repository:

```sh
git clone https://github.com/mrpatiwi/routed-react.git
cd routed-react
```

Install dependencies:

```sh
npm install
```

Start the project at [`http://localhost:9000`](http://localhost:9000).

```sh
npm start
```

### Running with Docker

Be sure to install Docker and start a Docker-machine if necessary.

Let's create an image named `routed-react`:

```sh
docker build -t routed-react .
```

Finally, start a container named `routed-react-instance` at port `80`.

```sh
docker run -p 80:9000 --name routed-react-instance routed-react
```

### Testing

```sh
npm test
```