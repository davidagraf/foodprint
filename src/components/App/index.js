// src/components/App/index.js
import React, { Component } from 'react';
import request from 'superagent';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Rectangle} from 'recharts';
import moment from 'moment';
import recipes from './recipes.js';

import './style.css';

const blackFoot = require('../../assets/foot.png');

const feet = [
	require('../../assets/foot_a.png'),
	require('../../assets/foot_b.png'),
	require('../../assets/foot_c.png'),
	require('../../assets/foot_d.png'),
	require('../../assets/foot_e.png')
];

const EATERNITY_URL = (process.env.NODE_ENV !== "production" && 'http://localhost:9000/co2' || '/co2');

function customBar(props) {
  let fill = '#000000';
  let rating = props.rating.toLowerCase();
  if (rating === 'a') {
    fill = '#1A9640';
  } else if (rating === 'b') {
    fill = '#A7D96A';
  }
  else if (rating === 'c') {
    fill = '#FFFFC3';
  }
  else if (rating === 'd') {
    fill = '#FDAE61';
  }
  else if (rating === 'e') {
    fill = '#DA1914';
  }
  return <Rectangle {...props} fill={fill} />;
}

class App extends Component {
  static propTypes = {}
  static defaultProps = {}
  state = {
		week: moment().startOf('isoweek')
	}

	_loadCO2() {
		const weekDate = this.state.week.format('YYYY-MM-DD');
		const data = recipes[weekDate];
		this.setState({waiting: true});
		request.post(EATERNITY_URL)
		.send(data)
		.end((err, res) => {
			this.setState({waiting: false});
		  this.setState({result: res.body});
		});
	}

	componentDidMount() {
		this._loadCO2();
	}

	_prevWeek() {
		this.setState({week: this.state.week.subtract(1, 'w')});
		this._loadCO2();
	}

	_nextWeek() {
		this.setState({week: this.state.week.add(1, 'w')});
		this._loadCO2();
	}

  render() {
		let averageRating = null;
		const codeBeforeA = 'a'.charCodeAt(0) - 1;
		if (this.state.result) {
			averageRating = this.state.result.reduce((sum, item) => {
				return sum + (item.rating.toLowerCase().charCodeAt(0) - codeBeforeA);
			}, 0);
			averageRating = feet[Math.round(averageRating / this.state.result.length) - 1] || blackFoot;
		}

    return (
      <div>
				{this.state.waiting && <div className="turningWheelModal"><div className="turningWheel"></div></div>}
				<div>
					<button onClick={() => this._prevWeek()}>Prev Week</button>
					<button onClick={() => this._nextWeek()}>Next Week</button>
				</div>
				<div>Week: {this.state.week.format('DD.MM.YYYY')}</div>
				{this.state.result && <div>
					Carbon Footprint: {this.state.result.reduce((sum, cur) => sum + cur.co2, 0)}g.
				</div>}
				{averageRating && <div>
					<img src={averageRating} />
				</div>}
        {this.state.result && <div>
				  <BarChart width={460} height={200} layout='vertical' data={this.state.result.sort((a,b)=>b.co2 - a.co2)}>
	          <Bar dataKey='co2' shape={customBar}/>
	          <YAxis type='category' dataKey="name" scaleToFit={true} width={150} />
	          <XAxis type='number'/>
          </BarChart>
				</div>}
      </div>
    );
  }
}

export default App;
