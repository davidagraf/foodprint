export default {
	'2017-01-23': [{
			name: 'Reis',
			amount: 1000
		}, {
			name: 'Nudel',
			amount: 100
		}, {
			name: 'Birne',
			amount: 100
		}, {
			name: 'Tomate',
			amount: 200
		}, {
			name: 'Brot',
			amount: 200
		}
	],
	'2017-01-30': [{
			name: 'Apfel',
			amount: 1000
		}, {
			name: 'Tomate',
			amount: 200
		}, {
			name: 'Wasser',
			amount: 1000
		}, {
			name: 'Essiggurke',
			amount: 200
		}
	],
	'2017-02-06': [{
			name: 'Tomate',
			amount: 200
		}, {
			name: 'Apfel',
			amount: 100
		}, {
			name: 'Pastinake',
			amount: 500
		}, {
			name: 'Gurke',
			amount: 200
		}, {
			name: 'Essiggurke',
			amount: 200
		}
	]
};
